#!/usr/bin/python

"""
Two SDR nodes running GNU Radio with an RF link.
"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn

IMAGE = 'urn:publicid:IDN+emulab.net+image+PowderTeam:U18-GR-PBUF'

portal.context.defineParameter("nuc_a_id",
                               "ID of NUC A",
                               portal.ParameterType.STRING,
                               "")
portal.context.defineParameter("nuc_b_id",
                               "ID of NUC B",
                               portal.ParameterType.STRING,
                               "")

params = portal.context.bindParameters()
request = portal.context.makeRequestRSpec()

node0 = request.RawPC( "nuc_a" )
#node0.hardware_type = "nuc5300"
node0.component_id = params.nuc_a_id
node0.disk_image = IMAGE
node0if1 = node0.addInterface( "n0rf0" )

node1 = request.RawPC( "nuc_b" )
#node1.hardware_type = "nuc5300"
node1.component_id = params.nuc_b_id
node1.disk_image = IMAGE
node1if = node1.addInterface( "n1rf0" )


rflink1 = request.RFLink( "rflink1" )
rflink1.addInterface( node0if1 )
rflink1.addInterface( node1if )

portal.context.printRequestRSpec()
